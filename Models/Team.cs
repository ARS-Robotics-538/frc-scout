using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using FrcScout;

namespace FrcScout.Models
{

	public class Team
	{
		public int Id { get; set; }				// UUID
		public int TeamNumber { get; set; }		// FRC Team Number
		public string Scouter { get; set; }		// User ID from Auth Provider
		public string Event { get; set; }		// FRC Event ID
		public string Match { get; set; }		// FRC Match Number
		//public EventList Event { get; set; }	// FRC Match Name
		//public FrcEvents Event { get; set; }

		// Autotomus Scoring
		public int AutoHighCube { get; set; }	// Autotomus Scoring High Cubes
		public int AutoHighCone { get; set; }	// Autotomus Scoring High Cones
		public int AutoMidCube { get; set; }	// Autotomus Scoring Mid Cubes
		public int AutoMidCone { get; set; }	// Autotomus Scoring Mid Cones
		public int AutoLowCube { get; set; }	// Autotomus Scoring Low Cubes
		public int AutoLowCone { get; set; }	// Autotomus Scoring Low Cones

		// Teleop Scoring
		public int TeleHighCube { get; set; }	// Teleop Scoring High Cubes
		public int TeleHighCone { get; set; }	// Teleop Scoring High Cones
		public int TeleMidCube { get; set; }	// Teleop Scoring Mid Cubes
		public int TeleMidCone { get; set; }	// Teleop Scoring Mid Cones
		public int TeleLowCube { get; set; }	// Teleop Scoring Low Cubes
		public int TeleLowCone { get; set; }	// Teleop Scoring Low Cones

		public int DefensiveManuevers { get; set; }	// Defensive Manuvers Performed
		public int Fouls { get; set; }			// Fouls count
		public int TechFouls { get; set; }		// Technical Fouls count
		public bool ChargeSupportedAuto { get; set; }	// Fully Supported by Charge Station in Auto
		public bool ChargeSupportedTele { get; set; }	// Fully Supported by Charge Station in Teleop
		public bool ChargeLevelAuto { get; set; }	// Leveled Charge Station in Auto
		public bool ChargeLevelTele { get; set; }	// Leveled Charge Station in Teleop
	}
}
