using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FrcScout.Models;

namespace FrcScout.Data
{
    public class FrcScoutContext : DbContext
    {
        public FrcScoutContext (DbContextOptions<FrcScoutContext> options)
            : base(options)
        {
        }

        public DbSet<FrcScout.Models.Team> Team { get; set; } = default!;
    }
}
