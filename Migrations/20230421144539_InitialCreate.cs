﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FrcScout.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Team",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TeamNumber = table.Column<int>(type: "INTEGER", nullable: false),
                    Scouter = table.Column<string>(type: "TEXT", nullable: false),
                    Event = table.Column<string>(type: "TEXT", nullable: false),
                    Match = table.Column<string>(type: "TEXT", nullable: false),
                    AutoHighCube = table.Column<int>(type: "INTEGER", nullable: false),
                    AutoHighCone = table.Column<int>(type: "INTEGER", nullable: false),
                    AutoMidCube = table.Column<int>(type: "INTEGER", nullable: false),
                    AutoMidCone = table.Column<int>(type: "INTEGER", nullable: false),
                    AutoLowCube = table.Column<int>(type: "INTEGER", nullable: false),
                    AutoLowCone = table.Column<int>(type: "INTEGER", nullable: false),
                    TeleHighCube = table.Column<int>(type: "INTEGER", nullable: false),
                    TeleHighCone = table.Column<int>(type: "INTEGER", nullable: false),
                    TeleMidCube = table.Column<int>(type: "INTEGER", nullable: false),
                    TeleMidCone = table.Column<int>(type: "INTEGER", nullable: false),
                    TeleLowCube = table.Column<int>(type: "INTEGER", nullable: false),
                    TeleLowCone = table.Column<int>(type: "INTEGER", nullable: false),
                    DefensiveManuevers = table.Column<int>(type: "INTEGER", nullable: false),
                    Fouls = table.Column<int>(type: "INTEGER", nullable: false),
                    TechFouls = table.Column<int>(type: "INTEGER", nullable: false),
                    ChargeSupportedAuto = table.Column<bool>(type: "INTEGER", nullable: false),
                    ChargeSupportedTele = table.Column<bool>(type: "INTEGER", nullable: false),
                    ChargeLevelAuto = table.Column<bool>(type: "INTEGER", nullable: false),
                    ChargeLevelTele = table.Column<bool>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Team", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Team");
        }
    }
}
