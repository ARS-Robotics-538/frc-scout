﻿// <auto-generated />
using FrcScout.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace FrcScout.Migrations
{
    [DbContext(typeof(FrcScoutContext))]
    [Migration("20230421144539_InitialCreate")]
    partial class InitialCreate
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.5");

            modelBuilder.Entity("FrcScout.Models.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoHighCone")
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoHighCube")
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoLowCone")
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoLowCube")
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoMidCone")
                        .HasColumnType("INTEGER");

                    b.Property<int>("AutoMidCube")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ChargeLevelAuto")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ChargeLevelTele")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ChargeSupportedAuto")
                        .HasColumnType("INTEGER");

                    b.Property<bool>("ChargeSupportedTele")
                        .HasColumnType("INTEGER");

                    b.Property<int>("DefensiveManuevers")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Event")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("Fouls")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Match")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<string>("Scouter")
                        .IsRequired()
                        .HasColumnType("TEXT");

                    b.Property<int>("TeamNumber")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TechFouls")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TeleHighCone")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TeleHighCube")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TeleLowCone")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TeleLowCube")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TeleMidCone")
                        .HasColumnType("INTEGER");

                    b.Property<int>("TeleMidCube")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("Team");
                });
#pragma warning restore 612, 618
        }
    }
}
