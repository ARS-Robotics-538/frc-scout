using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using FrcScout.Data;
using FrcScout.Models;

namespace FrcScout.Controllers
{
	public class ScoutController : Controller
	{
		private readonly FrcScoutContext _context;

		public ScoutController(FrcScoutContext context)
		{
			_context = context;
		}

		//
		// GET: /Scout/
		public string Index()
		{
			return "This is a placeholder for a scouting forms list.";
		}
		//
		// GET: /Scout/Pit
		public string Pit()
		{
			return "This is a placeholder for Pit Scouting.";
		}

		//
		// GET: /Scout/Match
		public IActionResult Match()
		{
			return View();
		}

		// POST: Team/Create
		// To protect from overposting attacks, enable the specific properties you want to bind to.
		// For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Create([Bind("Id,TeamNumber,Scouter,Event,Match,AutoHighCube,AutoHighCone,AutoMidCube,AutoMidCone,AutoLowCube,AutoLowCone,TeleHighCube,TeleHighCone,TeleMidCube,TeleMidCone,TeleLowCube,TeleLowCone,DefensiveManuevers,Fouls,TechFouls,ChargeSupportedAuto,ChargeSupportedTele,ChargeLevelAuto,ChargeLevelTele")] Team team)
		{
			if (ModelState.IsValid)
			{
				_context.Add(team);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			return View(team);
		}
	}
}
